package ru.itis.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.AuthorDto;
import ru.itis.services.BlogService;
import ru.itis.dto.PostDto;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 25.12.2021
 * 14. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RequiredArgsConstructor
@RestController
public class BlogController {
    private final BlogService blogService;

    @PostMapping(value = "/authors")
    public AuthorDto addAuthor(@RequestBody AuthorDto authorDto) {
        return blogService.addAuthor(authorDto);
    }

    @PostMapping(value = "/authors/{author-id}/posts")
    public PostDto addPost(@PathVariable("author-id") Long authorId, @RequestBody PostDto post) {
        return blogService.addPost(authorId, post);
    }

    @GetMapping("/posts")
    public List<PostDto> getByTitle(@RequestParam("title") String title) {
        return blogService.searchPostsByTitle(title);
    }
}
