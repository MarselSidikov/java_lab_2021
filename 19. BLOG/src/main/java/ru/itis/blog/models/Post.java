package ru.itis.blog.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

/**
 * 24.03.2022
 * 19. BLOG
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Data
@EqualsAndHashCode(exclude = {"author", "inFavorites"})
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Post {

    public enum State {
        DRAFT, DELETED, PUBLISHED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;

    private LocalDateTime createdAt;

    @Column(length = 20)
    private String title;

    @Column(length = 1000)
    private String text;

    @Enumerated(value = EnumType.STRING)
    private State state;

    @ManyToMany(mappedBy = "favorites")
    private Set<Author> inFavorites;


}
