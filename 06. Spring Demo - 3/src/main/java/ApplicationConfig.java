import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

/**
 * 11.09.2021
 * 06. Spring Demo - 3
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@PropertySource(value = "classpath:application.properties")
public class ApplicationConfig {

    @Autowired
    private Environment environment;

    @Bean
    public SignUpService signUpService(@Qualifier("passwordBlackListFile") PasswordBlackList passwordBlackList,
                                       EmailValidator emailValidator) {
        return new SignUpService(passwordBlackList, emailValidator);
    }

    @Bean
    public EmailValidator emailValidator() {
        EmailValidatorRegexImpl emailValidator =  new EmailValidatorRegexImpl();
        emailValidator.setRegex(environment.getProperty("emailValidator.regex"));
        return emailValidator;
    }

    @Bean
    public PasswordBlackList passwordBlackListHardCode() {
        return new PasswordBlackListHardCodeImpl();
    }

    @Bean
    public PasswordBlackList passwordBlackListFile() {
        return new PasswordBlackListFileImpl(environment.getProperty("passwordBlackList.file"));
    }


}
