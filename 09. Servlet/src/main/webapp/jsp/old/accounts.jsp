<%@ page import="ru.itis.models.Account" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table>
    <tr>
        <th>Id</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Age</th>
    </tr>
    <%
        List<Account> accounts = (List<Account>) request.getAttribute("accounts");
        for (Account account : accounts) {
    %>
    <tr>
        <td><%=account.getId()%></td>
        <td><%=account.getFirstName()%></td>
        <td><%=account.getLastName()%></td>
        <td><%=account.getAge()%></td>
    </tr>
    <%
        }
    %>
</table>
</body>
</html>
