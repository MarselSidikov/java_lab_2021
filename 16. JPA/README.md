# JPA

* Java Persistence API - реализация ORM в Java. Стандарт языка Java по тому, как должен выглядеть ORM. Представлен
интерфейсами и аннотациями.
* Hibernate - реализация JPA.

* `@Entity` - показывает, что объекты данного класса будут работать внутри ORM.
* `@Id`, `@GeneretaedValue`, `@ManyToOne`, `@JoinColumn`
* `EntityManager` - менеджер сущностей, интерфейс JPA, отвечает за манипуляцию сущностями.