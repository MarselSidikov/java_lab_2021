package ru.itis.blog.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.blog.models.Author;

/**
 * 24.03.2022
 * 19. BLOG
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface AuthorsRepository extends JpaRepository<Author, Long> {
    Page<Author> findAll(Pageable pageable);
}
