package ru.itis.app;

import org.springframework.core.env.PropertySource;
import org.springframework.core.io.support.ResourcePropertySource;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import ru.itis.config.ApplicationConfig;

import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.ServletRegistration.Dynamic;
import java.io.IOException;

/**
 * 13.11.2021
 * 14. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ApplicationInitializer implements WebApplicationInitializer {
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {

        // поднимаем конфигурацию контекста Spring для WebMvc приложений
        AnnotationConfigWebApplicationContext springWebContext = new AnnotationConfigWebApplicationContext();

//        PropertySource<?> propertySource = null;
//        try {
//            propertySource = new ResourcePropertySource("classpath:application.properties");
//        } catch (IOException e) {
//            throw new IllegalStateException(e);
//        }
        // регистрируем в этом контексте наш конфигурационный бин
//        springWebContext.getEnvironment().setActiveProfiles((String) propertySource.getProperty("spring.profile"));

        springWebContext.getEnvironment().setActiveProfiles(System.getenv().get("SPRING_PROFILE"));
        springWebContext.register(ApplicationConfig.class);
        // добавить Listener, который есть в Spring в контекст сервлетов
        ContextLoaderListener listener = new ContextLoaderListener(springWebContext);
        servletContext.addListener(listener);
        // создать DispatcherServlet во время запуска приложения и зарегистрировать его

        ServletRegistration.Dynamic dispatcherServlet = servletContext.addServlet("dispatcher",
                new DispatcherServlet(springWebContext));
        dispatcherServlet.setLoadOnStartup(1);
        dispatcherServlet.addMapping("/");
    }
}
