package ru.itis.education.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import ru.itis.education.models.Course;
import ru.itis.education.models.Student;
import ru.itis.education.repositories.CoursesRepositoryJpaImpl;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * 05.12.2021
 * 15. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();
        EntityManager entityManager = sessionFactory.createEntityManager();
        CoursesRepositoryJpaImpl coursesRepositoryJpa = new CoursesRepositoryJpaImpl(entityManager);
//        System.out.println(coursesRepositoryJpa.findAllByLesson_name("API"));
        coursesRepositoryJpa.save(Course.builder()
                        .name("DevOPS")
                        .description("Настройка CI/CD-окружения")
                .build());
    }
}
