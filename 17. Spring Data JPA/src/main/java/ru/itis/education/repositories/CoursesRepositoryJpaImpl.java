package ru.itis.education.repositories;

import org.springframework.stereotype.Repository;
import ru.itis.education.models.Course;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transaction;
import javax.transaction.Transactional;
import java.util.List;

/**
 * 11.12.2021
 * 16. JPA
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Repository
public class CoursesRepositoryJpaImpl implements CoursesRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    @Override
    public void save(Course course) {
        entityManager.persist(course);
    }

    @Override
    public List<Course> findAllByLesson_name(String name) {
        TypedQuery<Course> query = entityManager.createQuery("select course from Course course " +
                "left join course.lessons lesson where lesson.title = :name", Course.class);
        query.setParameter("name", name);
        return query.getResultList();
    }
}
