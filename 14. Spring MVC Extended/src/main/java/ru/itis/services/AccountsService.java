package ru.itis.services;

import ru.itis.dto.AccountDto;

import java.util.List;

/**
 * 06.11.2021
 * 13. Spring MVC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface AccountsService {
    List<AccountDto> getAccounts();

    AccountDto getAccount(Long accountId);

    void addAccount(AccountDto account);
}
