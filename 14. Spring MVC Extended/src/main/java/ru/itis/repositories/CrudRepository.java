package ru.itis.repositories;

import ru.itis.models.Account;

import java.util.List;
import java.util.Optional;

/**
 * 27.11.2021
 * 14. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CrudRepository<T> {
    List<T> findAll();

    void save(T entity);

    Optional<T> findById(Long accountId);
}
