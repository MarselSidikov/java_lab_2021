package ru.itis;

import ru.itis.models.Account;
import ru.itis.models.Car;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;
import java.util.function.Function;

/**
 * 03.07.2021
 * 01. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class AccountsRepositoryJdbcImpl implements AccountsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select c.id as car_id, * from account a left join car c on c.owner_id = a.id order by a.id";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_FIRST_NAME = "select * from account where first_name = ?";

    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update account set first_name = ?, last_name = ?, age = ? where id = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select *, c.id as car_id from account left join car c on account.id = c.owner_id where account.id = ?";

    private final DataSource dataSource;

    public AccountsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private final Function<ResultSet, Account> accountRowMapper = row -> {
        try {
            int id = row.getInt("id");
            String firstName = row.getString("first_name");
            String lastName = row.getString("last_name");
            int age = row.getInt("age");

            Account account = new Account(id, firstName, lastName, age);
            account.setCars(new ArrayList<>());
            return account;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    private final Function<ResultSet, Car> carRowMapper = row -> {
        try {
            int id = row.getInt("car_id");
            String color = row.getString("color");
            String model = row.getString("model");
            return new Car(id, color, model);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    @Override
    public List<Account> findAll() {
        List<Account> accounts = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet rows = statement.executeQuery(SQL_SELECT_ALL)) {

            Set<Integer> processedAccounts = new HashSet<>();
            Account currentAccount = null;

            while (rows.next()) {
                // если мы еще не обрабатывали владельца, то запоминаем
                if (!processedAccounts.contains(rows.getInt("id"))) {
                    currentAccount = accountRowMapper.apply(rows);
                    // кладем его в список аккаунтов
                    accounts.add(currentAccount);
                }
                // получили текущего владельца

                Integer carId = rows.getObject("car_id", Integer.class);

                // значит у текущего пользователя есть машина
                if (carId != null) {
                    // нужно получить эту машину
                    Car car = carRowMapper.apply(rows);
                    // задали ей владельца
                    car.setOwner(currentAccount);
                    // положили машину этому владельцу
                    currentAccount.getCars().add(car);
                }
                processedAccounts.add(currentAccount.getId());
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return accounts;
    }

    @Override
    public List<Account> findAllByFirstName(String searchFirstName) {
        List<Account> accounts = new ArrayList<>();
        try {
            Connection connection = dataSource.getConnection();

            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_BY_FIRST_NAME);
            statement.setString(1, searchFirstName);

            ResultSet rows = statement.executeQuery();

            while (rows.next()) {
                accounts.add(accountRowMapper.apply(rows));
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return accounts;
    }

    @Override
    public Optional<Account> findById(Integer id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {

            statement.setInt(1, id);

            try (ResultSet row = statement.executeQuery()) {
                if (row.next()) {
                    // получили самого пользователя
                    Account account = accountRowMapper.apply(row);
                    // теперь нужно вытащить все его машины
                    do {
                        // получили машину
                        Car car = carRowMapper.apply(row);
                        // задали ей владельца
                        car.setOwner(account);
                        // положили машину пользователю
                        account.getCars().add(car);
                    } while (row.next());
                    return Optional.of(account);
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public List<Account> findAllByFirstNameOrLastNameLike(String name) {
        return null;
    }

    @Override
    public void save(Account account) {

    }

    @Override
    public void update(Account account) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_BY_ID)) {

            statement.setString(1, account.getFirstName());
            statement.setString(2, account.getLastName());
            statement.setInt(3, account.getAge());
            statement.setInt(4, account.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Exception in <Update>");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
