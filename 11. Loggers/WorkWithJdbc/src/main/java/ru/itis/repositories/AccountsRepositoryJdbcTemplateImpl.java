package ru.itis.repositories;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import ru.itis.models.Account;

import javax.sql.DataSource;
import java.util.List;

/**
 * 03.07.2021
 * 01. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class AccountsRepositoryJdbcTemplateImpl implements AccountsRepository {

    private static final Logger logger = LoggerFactory.getLogger(AccountsRepositoryJdbcTemplateImpl.class);

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from account order by id";

    private JdbcTemplate jdbcTemplate;

    public AccountsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private final RowMapper<Account> accountRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
            String firstName = row.getString("first_name");
            String lastName = row.getString("last_name");
            int age = row.getInt("age");

        return new Account(id, firstName, lastName, age);
    };

    @Override
    public List<Account> findAll() {
        logger.info("Send query to database...");
        return jdbcTemplate.query(SQL_SELECT_ALL, accountRowMapper);
    }
}
