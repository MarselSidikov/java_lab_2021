package ru.itis.education.app;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.itis.education.models.Course;
import ru.itis.education.models.Student;
import ru.itis.education.repositories.CoursesRepositoryJpaImpl;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * 11.12.2021
 * 16. JPA
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();
        EntityManager entityManager = sessionFactory.createEntityManager();

        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityManager.getTransaction();

        entityTransaction.begin();
        Student marsel = entityManager.find(Student.class, 1L);
        Student alexander = entityManager.find(Student.class, 2L);
        Student daniil = entityManager.find(Student.class, 3L);

        Course course = entityManager.find(Course.class, 4L);

//        course.getStudents().add(marsel);
//        course.getStudents().add(alexander);
//        course.getStudents().add(daniil);

        marsel.getCourses().add(course);
        alexander.getCourses().add(course);
        daniil.getCourses().add(course);

        entityManager.persist(marsel);
        entityManager.persist(alexander);
        entityManager.persist(daniil);
        entityTransaction.commit();
    }
}
