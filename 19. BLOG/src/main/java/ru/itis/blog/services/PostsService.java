package ru.itis.blog.services;

import ru.itis.blog.dto.AddPostDto;
import ru.itis.blog.dto.PostDto;

/**
 * 31.03.2022
 * 19. BLOG
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface PostsService {
    PostDto getPost(Long postId);

    PostDto addPost(AddPostDto post);
}
