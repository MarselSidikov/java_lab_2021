package ru.itis.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * 11.09.2021
 * 08. Spring Demo - 5
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Component("passwordBlackListDb")
public class PasswordBlackListJdbcImpl implements PasswordBlackList {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PasswordBlackListJdbcImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public boolean contains(String password) {
        try {
            System.out.println(jdbcTemplate.getDataSource().getConnection().getSchema());
        } catch (SQLException throwables) {
            throw new IllegalArgumentException(throwables);
        }
        return false;
    }
}
