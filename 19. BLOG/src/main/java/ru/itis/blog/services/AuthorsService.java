package ru.itis.blog.services;

import org.springframework.data.domain.Page;
import ru.itis.blog.dto.AddToFavoritesDto;
import ru.itis.blog.dto.AuthorDto;
import ru.itis.blog.dto.AuthorsPage;
import ru.itis.blog.models.Author;

import java.util.List;

/**
 * 24.03.2022
 * 19. BLOG
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface AuthorsService {
    AuthorsPage getAuthors(int page);

    AuthorDto addAuthor(AuthorDto author);

    AuthorDto updateAuthor(Long authorId, AuthorDto newData);

    void addPostToFavorites(Long authorId, AddToFavoritesDto post);

    void deletePostFromFavorites(Long authorId, Long postId);
}
