import java.time.LocalDateTime;
import java.util.StringJoiner;

/**
 * 04.09.2021
 * 03. Reflection
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Certificate {
    private LocalDateTime from;
    private String name;
    private int count;

    public Certificate(LocalDateTime from, String name, Integer count) {
        this.from = from;
        this.name = name;
        this.count = count;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Certificate.class.getSimpleName() + "[", "]")
                .add("from=" + from)
                .add("name='" + name + "'")
                .add("count=" + count)
                .toString();
    }
}
