package ru.itis.blog.controllers;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.web.servlet.MockMvc;
import ru.itis.blog.dto.AuthorDto;
import ru.itis.blog.dto.AuthorsPage;
import ru.itis.blog.services.AuthorsService;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * 07.05.2022
 * 19. BLOG
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@WebMvcTest(AuthorsController.class)
@DisplayName("AuthorsController is working when ...")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class AuthorsControllerTest {

    private static final AuthorDto AUTHOR_MARSEL = AuthorDto.builder()
            .id(1L).firstName("Марсель").lastName("Сидиков").build();

    private static final AuthorDto AUTHOR_KING = AuthorDto.builder()
            .id(2L).firstName("Стивен").lastName("Кинг").build();

    private static final List<AuthorDto> AUTHORS_LIST = Arrays.asList(AUTHOR_MARSEL, AUTHOR_KING);

    private static final AuthorDto NEW_AUTHOR = AuthorDto.builder()
            .firstName("marsel").lastName("sidikov").build();

    private static final AuthorDto CREATED_AUTHOR = AuthorDto.builder()
            .id(777L).firstName("marsel").lastName("sidikov").build();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthorsService authorsService;

    @MockBean
    private UserDetailsService userDetailsService;

    @Value("${test.token}")
    private String token;

    @BeforeEach
    void setUp() {
        when(authorsService.getAuthors(0)).thenReturn(AuthorsPage.builder()
                .authors(AUTHORS_LIST).totalPages(1).build());

        when(authorsService.addAuthor(NEW_AUTHOR)).thenReturn(CREATED_AUTHOR);
    }

    @Test
    public void return_403_without_token() throws Exception {
        mockMvc.perform(get("/authors")
                        .param("page", "1"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void return_authors_on_0_page() throws Exception {
        mockMvc.perform(get("/authors")
                        .param("page", "0")
                        .header("Authorization", token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("authors", hasSize(2)))
                .andExpect(jsonPath("totalPages", is(1)))
                .andExpect(jsonPath("authors[0].firstName", is("Марсель")))
                .andExpect(jsonPath("authors[0].lastName", is("Сидиков")))
                .andExpect(jsonPath("authors[1].firstName", is("Стивен")))
                .andExpect(jsonPath("authors[1].lastName", is("Кинг")));
    }

    @Test
    public void return_400_when_add_author_with_same_names() throws Exception {
        mockMvc.perform(post("/authors")
                        .header("Authorization", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"firstName\": \"same\",\n" +
                                "  \"lastName\": \"same\"\n" +
                                "}"))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void add_author_with_valid_names() throws Exception {
        mockMvc.perform(post("/authors")
                        .header("Authorization", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"firstName\": \"marsel\",\n" +
                                "  \"lastName\": \"sidikov\"\n" +
                                "}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id", is(777)))
                .andExpect(jsonPath("firstName", is("marsel")))
                .andExpect(jsonPath("lastName", is("sidikov")))
                .andDo(print());
    }


}