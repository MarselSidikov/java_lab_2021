package ru.itis.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.AccountDto;
import ru.itis.repositories.AccountsRepository;

import java.util.List;

/**
 * 20.11.2021
 * 14. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
@RequiredArgsConstructor
public class SearchServiceImpl implements SearchService {

    private final AccountsRepository accountsRepository;

    @Override
    public List<AccountDto> searchUsersByEmail(String email) {
        return accountsRepository.searchByEmail(email);
    }
}
