package ru.itis.repositories;

import ru.itis.dto.AccountDto;
import ru.itis.models.Account;

import java.util.List;
import java.util.Optional;

/**
 * 03.07.2021
 * 01. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface AccountsRepository extends CrudRepository<Account> {

    Optional<Account> findByEmail(String email);

    List<AccountDto> searchByEmail(String email);
}
