package ru.itis.blog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.blog.models.Post;

/**
 * 24.03.2022
 * 19. BLOG
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface PostsRepository extends JpaRepository<Post, Long> {
}
