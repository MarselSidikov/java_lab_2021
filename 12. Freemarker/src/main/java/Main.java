import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.File;
import java.io.FileWriter;
import java.io.FilterWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 27.10.2021
 * 12. Freemarker
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_21);
        configuration.setDefaultEncoding("UTF-8");

        configuration.setTemplateLoader(new FileTemplateLoader(new File("src/main/resources")));
        Template template = configuration.getTemplate("template_for_web.ftlh");

        User user = User.builder()
                .id(10L)
                .name("Марсель")
                .age(27)
                .build();

        List<User> users = new ArrayList<>();
        users.add(User.builder().age(26).name("Игорь").build());
        users.add(User.builder().age(33).name("Слава").build());
        users.add(User.builder().age(54).name("Стас").build());

        Map<String, Object> attributes = new HashMap<>();
        attributes.put("user", user);
        attributes.put("users", users);

        FileWriter writer = new FileWriter("output.html");
        template.process(attributes, writer);

    }
}
