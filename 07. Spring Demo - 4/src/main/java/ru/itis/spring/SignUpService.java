package ru.itis.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * 04.09.2021
 * 04. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Component
public class SignUpService {
    private final PasswordBlackList passwordBlackList;

    private final EmailValidator emailValidator;

    @Autowired
    public SignUpService(@Qualifier("passwordBlackListFile") PasswordBlackList passwordBlackList, EmailValidator emailValidator) {
        this.passwordBlackList = passwordBlackList;
        this.emailValidator = emailValidator;
    }

    public void signUp(String email, String password) {

        if (emailValidator.isValid(email)) {
            System.out.println("Email хороший");
        } else {
            System.err.println("Email не подходит");
        }

        if (passwordBlackList.contains(password)) {
            System.err.println("Пароль не подходит!");
        } else {
            System.out.println("Пароль хороший");
        }
    }
}
