import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * 04.09.2021
 * 03. Reflection
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ObjectsGenerator {
    public <T> T generate(Class<T> objectClass, Object ... properties) {

        Class<?>[] typesOfProperties = getTypesOfProperties(properties);

        try {
            Constructor<T> constructor = objectClass.getConstructor(typesOfProperties);
            return constructor.newInstance(properties);
        } catch (ReflectiveOperationException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Class<?>[] getTypesOfProperties(Object[] properties) {
        List<Class<?>> typesOfProperties = new ArrayList<>();
        for (Object property : properties) {
            typesOfProperties.add(property.getClass());
        }

        Class<?>[] typesOfPropertiesAsArray = new Class[typesOfProperties.size()];
        typesOfProperties.toArray(typesOfPropertiesAsArray);

        return typesOfPropertiesAsArray;
    }
}
