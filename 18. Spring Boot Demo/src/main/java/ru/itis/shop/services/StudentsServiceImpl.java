package ru.itis.shop.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.shop.dto.StudentDto;
import ru.itis.shop.repositories.StudentsRepository;

import java.util.List;

import static ru.itis.shop.dto.StudentDto.from;

/**
 * 17.02.2022
 * 18. Spring Boot Demo
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@Service
public class StudentsServiceImpl implements StudentsService {

    private final StudentsRepository studentsRepository;

    @Override
    public List<StudentDto> getAll() {
        return from(studentsRepository.findAll());
    }
}
