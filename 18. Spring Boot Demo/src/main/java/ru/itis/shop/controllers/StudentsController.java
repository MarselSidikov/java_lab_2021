package ru.itis.shop.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.itis.shop.services.StudentsService;

/**
 * 17.02.2022
 * 18. Spring Boot Demo
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@Controller
@RequestMapping("/students")
public class StudentsController {

    private final StudentsService studentsService;

    @GetMapping
    public String getStudentsPage(Model model) {
        model.addAttribute("students", studentsService.getAll());
        return "students";
    }
}
