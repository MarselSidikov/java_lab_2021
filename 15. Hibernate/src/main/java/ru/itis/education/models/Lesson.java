package ru.itis.education.models;

import lombok.*;

/**
 * 05.12.2021
 * 15. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = "course")
@EqualsAndHashCode(exclude = "course")
public class Lesson {
    private Long id;

    private String title;

    private Course course;
}
