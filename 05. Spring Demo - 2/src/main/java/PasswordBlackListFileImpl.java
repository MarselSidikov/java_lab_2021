import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * 04.09.2021
 * 04. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class PasswordBlackListFileImpl implements PasswordBlackList {

    private String fileName;

    public PasswordBlackListFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public boolean contains(String password) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().anyMatch(line -> line.equals(password));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
