package ru.itis.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.CourseDto;
import ru.itis.models.Course;
import ru.itis.repositories.CoursesRepository;

import java.util.List;

import static ru.itis.dto.CourseDto.from;

/**
 * 18.12.2021
 * 14. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RequiredArgsConstructor
@Service
public class CoursesServiceImpl implements CoursesService {

    private final CoursesRepository coursesRepository;

    @Override
    public List<CourseDto> getCoursesByLessonName(String lessonName) {
        return from(coursesRepository.findAllByLesson_name(lessonName));
    }

    @Override
    public CourseDto addCourse(CourseDto course) {
        Course newCourse = Course.builder()
                .name(course.getName())
                .description(course.getDescription())
                .build();
        return from(coursesRepository.save(newCourse));
    }
}
