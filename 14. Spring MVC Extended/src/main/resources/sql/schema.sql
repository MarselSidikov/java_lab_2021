create table account (
    id integer primary key auto_increment,
    first_name varchar(20),
    last_name varchar(20),
    password varchar(20),
    email varchar(20),
    age integer
);