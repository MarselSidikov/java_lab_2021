package ru.itis.education.repositories;

import ru.itis.education.models.Course;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import javax.transaction.Transaction;
import java.util.List;

/**
 * 11.12.2021
 * 16. JPA
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class CoursesRepositoryJpaImpl {
    private final EntityManager entityManager;

    public CoursesRepositoryJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void save(Course course) {
        EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();
        entityManager.persist(course);
        transaction.commit();
    }

    public List<Course> findAllByLesson_name(String name) {
        TypedQuery<Course> query = entityManager.createQuery("select course from Course course " +
                "left join course.lessons lesson where lesson.title = :name", Course.class);
        query.setParameter("name", name);
        return query.getResultList();
    }
}
