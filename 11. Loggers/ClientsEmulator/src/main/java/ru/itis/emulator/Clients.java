package ru.itis.emulator;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 13.10.2021
 * clients-emulator
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Clients {

    private static final int TOO_MANY_CLIENTS = 3;

    private static final Logger logger = Logger.getLogger(Clients.class);

    private final List<Runnable> clients;

    private final ExecutorService executorService;

    public Clients() {
        this.clients = new ArrayList<>();
        this.executorService = Executors.newCachedThreadPool();
        logger.info("ThreadPool initialized");
    }

    public void newClient(Runnable client) {
        this.clients.add(client);
        logger.debug("Client added");

        if (this.clients.size() > TOO_MANY_CLIENTS) {
            logger.warn("Too many clients");
        }

    }

    public void run() {
        for (Runnable client : clients) {
            executorService.submit(client);
        }
        logger.info("Run clients");
    }

    public void stop() {
        executorService.shutdown();
        logger.info("ThreadPool shutdown");
    }
}
