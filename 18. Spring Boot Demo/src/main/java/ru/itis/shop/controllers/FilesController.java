package ru.itis.shop.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.shop.dto.FileLinkDto;
import ru.itis.shop.services.FilesService;

import javax.servlet.http.HttpServletResponse;

/**
 * 17.02.2022
 * 18. Spring Boot Demo
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@Controller
@RequestMapping("/files")
public class FilesController {

    private final FilesService filesService;

    @GetMapping
    public String getFilesUploadPage() {
        return "file_upload_page";
    }

    @PostMapping("/upload")
    @ResponseBody
    public ResponseEntity<FileLinkDto> upload(@RequestParam("file") MultipartFile multipart, @RequestParam("description") String description) {
        return ResponseEntity.ok(filesService.upload(multipart, description));
    }

    @GetMapping("/{file-name:.+}")
    public void getFile(@PathVariable("file-name") String fileName, HttpServletResponse response) {
        filesService.addFileToResponse(fileName, response);
    }
}
