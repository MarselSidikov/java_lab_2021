package ru.itis.plugins.mojo;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * 14.05.2022
 * 20. Code Processor Plugin
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Mojo(name = "size-of-source-code", defaultPhase = LifecyclePhase.COMPILE)
public class SizeOfSourceCodeMojo extends AbstractMojo {
    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    private MavenProject project;

    @Parameter(defaultValue = "${project.build.outputDirectory}", required = true)
    private String outputDirectoryFileName;

    @Parameter(name = "fileForSizeValueFileName", required = true)
    private String fileForSizeValueFileName;


    @Override
    public void execute() throws MojoExecutionException {
        File outputDirectory = new File(outputDirectoryFileName);

        File listOfClassesFile = new File(outputDirectory, fileForSizeValueFileName);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(listOfClassesFile))) {
            getLog().info("Output file for size value is - " + fileForSizeValueFileName);

//             String sourceDirectoryFileName = project.getBuild().getScriptSourceDirectory();
            String sourceDirectoryFileName = project.getBuild().getSourceDirectory();

            long size = Files.walk(Paths.get(sourceDirectoryFileName))
                    .filter(Files::isRegularFile)
                    .map(file -> {
                        try {
                            return Files.size(file);
                        } catch (IOException e) {
                            throw new IllegalArgumentException(e);
                        }
                    }).mapToLong(Long::longValue)
                    .sum();

            writer.write(String.valueOf(size));
            getLog().info("Finish work");
        } catch (IOException e) {
            throw new MojoExecutionException(e);
        }
    }
}
