package ru.itis.blog.dto;

import lombok.Data;

/**
 * 16.04.2022
 * 19. BLOG
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Data
public class EmailPasswordDto {
    private String email;
    private String password;
}
