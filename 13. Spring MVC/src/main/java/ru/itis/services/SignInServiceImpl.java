package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.dto.SignInForm;
import ru.itis.models.Account;
import ru.itis.repositories.AccountsRepository;

import java.util.Optional;

/**
 * 16.10.2021
 * 09. Servlet
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
public class SignInServiceImpl implements SignInService {

    private final AccountsRepository accountsRepository;

    @Autowired
    public SignInServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public boolean signIn(SignInForm signInForm) {
        Optional<Account> accountOptional = accountsRepository.findByEmail(signInForm.getEmail());

        if (!accountOptional.isPresent()) {
            return false;
        }

        Account account = accountOptional.get();

        return account.getPassword().equals(signInForm.getPassword());
    }
}
