package ru.itis.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 08.10.2021
 * 09. Servlet
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebFilter("/*")
public class ColorFilter implements Filter {

    private final static String COLOR_PARAMETER_NAME = "color";

    private final static String COLOR_ATTRIBUTE_NAME = "color";

    private final static String PAGE_COLOR_COOKIE_NAME = "pageColor";

    private final static int COLOR_COOKIE_MAX_AGE = 60 * 60 * 24 * 365;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse)servletResponse;

        String color = request.getParameter(COLOR_PARAMETER_NAME);

        // если параметр был задан
        if (color != null) {
            createColorCookie(response, color);
        } else {
            color = processColorCookie(request);

        }
        // в качестве атрибута запроса я кладу либо значение из параметра либо из куки
        request.setAttribute(COLOR_ATTRIBUTE_NAME, color);

        chain.doFilter(request, response);
    }

    private String processColorCookie(HttpServletRequest request) {
        String color = null;
        // если у меня не было параметра
        // то возможно информация есть в куках
        Cookie[] cookies = request.getCookies();
        // пробега по всем кукам
        for (Cookie cookie : cookies) {
            // нашел нужную
            if (cookie.getName().equals(PAGE_COLOR_COOKIE_NAME)) {
                // запомнил цвет
                color = cookie.getValue();
            }
        }
        return color;
    }

    private void createColorCookie(HttpServletResponse response, String color) {
        // я просто создам куку и добавлю ее в ответ
        Cookie colorCookie = new Cookie(PAGE_COLOR_COOKIE_NAME, color);
        colorCookie.setMaxAge(COLOR_COOKIE_MAX_AGE);
        response.addCookie(colorCookie);
    }

    @Override
    public void destroy() {

    }
}
