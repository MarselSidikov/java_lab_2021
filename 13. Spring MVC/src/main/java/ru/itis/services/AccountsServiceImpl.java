package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.dto.AccountDto;
import ru.itis.repositories.AccountsRepository;

import java.util.List;

import static ru.itis.dto.AccountDto.from;

/**
 * 06.11.2021
 * 13. Spring MVC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
public class AccountsServiceImpl implements AccountsService {

    private final AccountsRepository accountsRepository;

    @Autowired
    public AccountsServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public List<AccountDto> getAccounts() {
        return from(accountsRepository.findAll());
    }
}
