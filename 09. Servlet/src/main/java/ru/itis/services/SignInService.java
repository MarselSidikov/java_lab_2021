package ru.itis.services;

import ru.itis.dto.SignInForm;

/**
 * 16.10.2021
 * 09. Servlet
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface SignInService {
    boolean signIn(SignInForm signInForm);
}
