package ru.itis.blog.dto;

import lombok.Data;

/**
 * 31.03.2022
 * 19. BLOG
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Data
public class AddToFavoritesDto {
    private Long postId;
}
