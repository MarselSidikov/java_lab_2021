package ru.itis.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.CarDto;
import ru.itis.models.Car;
import ru.itis.repositories.CarsRepository;

import java.util.List;

import static ru.itis.dto.CarDto.from;

/**
 * 27.11.2021
 * 14. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RequiredArgsConstructor
@Service
public class CarsServiceImpl implements CarsService {
    private final CarsRepository carsRepository;

    @Override
    public List<CarDto> addCar(CarDto car) {
        Car newCar = Car.builder()
                .color(car.getColor())
                .model(car.getModel())
                .build();

        carsRepository.save(newCar);

        return from(carsRepository.findAll());
    }

    @Override
    public List<CarDto> getAll() {
        return from(carsRepository.findAll());
    }
}
