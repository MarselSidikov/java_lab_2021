/**
 * 04.09.2021
 * 04. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
//        PasswordBlackList passwordBlackList = new PasswordBlackListHardCodeImpl();
        PasswordBlackList passwordBlackList = new PasswordBlackListFileImpl("passwords.txt");
        SignUpService service = new SignUpService(passwordBlackList, null);
        service.signUp("sidikovmarsel@gmail.com", "simple-dimple");
    }
}
