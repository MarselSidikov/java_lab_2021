package ru.itis.shop.dto;

import lombok.Data;

/**
 * 24.02.2022
 * 18. Spring Boot Demo
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Data
public class SignUpDto {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
