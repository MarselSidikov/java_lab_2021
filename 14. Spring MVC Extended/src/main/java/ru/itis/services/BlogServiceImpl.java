package ru.itis.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.AuthorDto;
import ru.itis.dto.PostDto;
import ru.itis.models.Author;
import ru.itis.models.Post;
import ru.itis.repositories.AuthorsRepository;
import ru.itis.repositories.PostsRepository;

import java.time.LocalDateTime;
import java.util.List;

import static ru.itis.dto.PostDto.from;
import static ru.itis.dto.AuthorDto.from;

/**
 * 25.12.2021
 * 14. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RequiredArgsConstructor
@Service
public class BlogServiceImpl implements BlogService {

    private final PostsRepository postsRepository;

    private final AuthorsRepository authorsRepository;

    @Override
    public PostDto addPost(Long authorId, PostDto post) {
        Author author = authorsRepository.getById(authorId);
        Post newPost = Post.builder()
                .publishedTime(LocalDateTime.now())
                .title(post.getTitle())
                .text(post.getText())
                .author(author)
                .build();
        return from(postsRepository.save(newPost));
    }

    @Override
    public AuthorDto addAuthor(AuthorDto author) {
        Author newAuthor = Author.builder()
                .firstName(author.getFirstName())
                .lastName(author.getLastName())
                .build();
        return from(authorsRepository.save(newAuthor));

    }

    @Override
    public List<PostDto> searchPostsByTitle(String title) {
        return from(postsRepository.findAllByTitleLike("%" + title + "%"));
    }
}
