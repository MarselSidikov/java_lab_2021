# Spring Boot

* `Spring Boot` - фреймворк, построенный над Spring (переосмысление Spring). Добавляет широкие возможности автоконфигурирования.

* `application.properties` - файл с параметрами компонентов приложения. Большое количество параметров уже зарезервировано и
используется в автоматическом режиме. Например, параметры `spring.datasource.*` используются для настройки подключения к БД.

* `CommandLineRunner` - интерфейс, реализация которого (если она объявлена как бин) автоматически подхватывается Spring Boot
и используется как логика для работы после запуска.

## Разбор принципов работы Spring Boot на примере настроек БД

* Стартер - обычный `pom.xml` с нужными зависимостями.

* `spring-boot-starter-data-jpa` - специальный стартер для настроек работы через Spring Data JPA
  * `spring-boot-starter-jdbc` - специальный стартер для настроек работы через JDBC
    * `spring-boot-starter` - содержит необходимые зависимости для работы Spring Boot
      * `spring-boot-autoconfigure` - механизмы автоконфигурации
    * `HikariCP` - Hikari Connection Pool
    * `spring-jdbc` - `JdbcTemplate`, `NamedParameterJdbcTemplate`, `RowMapper` и т.д.
  * `jakarta.transaction-api` - JTA
  * `jakarta.persistence-api` - JPA
  * `hibernate-core` - HIBERNATE
  * `spring-data-jpa` - `JpaRepository`

* `spring.factories` из `spring-boot-autoconfigure` - файл, в котором перечислены классы для автоконфигурации. Например `org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration`.
* `DataSourceConfiguration` - класс, который позволяет настроить `DataSource`, как он понимает, какой тип `DataSource` использовать?
  * `@ConditionalOnClass(HikariDataSource.class)` - данная аннотация говорит, что если в `classpath` есть `HikariDataSource`, то нужно активировать бин из класса, которым помечена эта аннотация.
* Почему `DataSourceConfiguration` вообще отрабатывает?
  * `@Import({ DataSourceConfiguration.Hikari.class..` - данная аннотация комбинирует конфигурацию из `DataSourceAutoConfiguration` и `Hikari` из `DataSourceConfiguration`.
* Почему он забрал свойства из `application.properties`?
  * `@ConfigurationProperties(prefix = "spring.datasource") public class DataSourceProperties` - данная аннотация говорит, что все свойства, которые начинаются с `spring.datasource` нужно вставить в поля класса `DataSourceProperties`

```
spring-boot-autoconfigure - > spring.factories -> DataSourceAutoConfiguration -> DataSourceProperties -> DataSourceConfiguration
```

## Важные моменты

* Встроенный Tomcat
* Профили - реализуются через разные properties-файлы, само значение текущего профиля указывается в параметрах запуска приложения:

```
java -Dspring.profiles.active=production ru.itis.shop.Application
```