package ru.itis.services;

import ru.itis.dto.CarDto;

import java.util.List;

/**
 * 27.11.2021
 * 14. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CarsService {
    List<CarDto> addCar(CarDto car);
    List<CarDto> getAll();
}
