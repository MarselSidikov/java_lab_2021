package ru.itis;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.itis.emulator.Clients;
import ru.itis.repositories.AccountsRepository;
import ru.itis.repositories.AccountsRepositoryJdbcTemplateImpl;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * 13.10.2021
 * WorkWithJdbc
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Properties environment = new Properties();
        try {
            environment.load(ClassLoader.getSystemResourceAsStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(environment.getProperty("db.url"));
        config.setUsername(environment.getProperty("db.user"));
        config.setPassword(environment.getProperty("db.password"));
        config.setDriverClassName(environment.getProperty("db.driver"));
        config.setMaximumPoolSize(Integer.parseInt(environment.getProperty("db.hikari.pool-size")));

        AccountsRepository accountsRepository =
                new AccountsRepositoryJdbcTemplateImpl(new HikariDataSource(config));
        Clients clients = new Clients();
        clients.newClient(() -> System.out.println(accountsRepository.findAll()));
        clients.newClient(() -> System.out.println(accountsRepository.findAll()));
        clients.newClient(() -> System.out.println(accountsRepository.findAll()));
        clients.newClient(() -> System.out.println(accountsRepository.findAll()));
        clients.newClient(() -> System.out.println(accountsRepository.findAll()));
        clients.run();
        clients.stop();

    }
}
