package ru.itis.emulator;

import java.util.Scanner;

/**
 * 13.10.2021
 * clients-emulator
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Clients clients = new Clients();
        clients.newClient(() -> System.out.println("Hello!"));
        clients.newClient(() -> System.out.println("Hello!"));
        clients.newClient(() -> System.out.println("Hello!"));
        clients.newClient(() -> System.out.println("Hello!"));
        clients.newClient(() -> System.out.println("Hello!"));
        clients.newClient(() -> System.out.println("Hello!"));
        clients.newClient(() -> System.out.println("Hello!"));
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        clients.run();
        clients.stop();

    }
}
