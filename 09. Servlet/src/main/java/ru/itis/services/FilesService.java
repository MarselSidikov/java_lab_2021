package ru.itis.services;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * 20.10.2021
 * 09. Servlet
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface FilesService {
    void upload(String fileName, InputStream fileInputStream);
    void download(String fileName, OutputStream responseOutputStream);
}
