# Spring - Часть 5

* Все варианты конфигураций:

1. Чистый XML + application.properties
2. Использование `@Autowired`, связи не указываем в xml.
3. Использование `@Bean` и `@Value`
4. Использование `@Component`
5. Комбинация `@Autorwired`, `@Bean`, `@Value`, `@ComponentScan` без xml и с `AnnotationConfigApplicationContext`