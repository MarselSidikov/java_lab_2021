package ru.itis.shop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.itis.shop.models.FileInfo;

import java.util.Optional;

/**
 * 20.03.2022
 * 18. Spring Boot Demo
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface FilesInfoRepository extends JpaRepository<FileInfo, Long> {
    Optional<FileInfo> findByStorageFileName(String fileName);
}
