package ru.itis.services;

import ru.itis.dto.AccountDto;

import java.util.List;

/**
 * 20.11.2021
 * 14. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface SearchService {
    List<AccountDto> searchUsersByEmail(String email);
}
