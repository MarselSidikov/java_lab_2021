package ru.itis.models;

import lombok.*;

import java.util.List;
import java.util.StringJoiner;

/**
 * 03.07.2021
 * 01. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Account {
    private Integer id;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private Integer age;
}
