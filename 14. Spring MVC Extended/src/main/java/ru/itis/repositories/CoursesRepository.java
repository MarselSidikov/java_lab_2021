package ru.itis.repositories;

import ru.itis.dto.CourseDto;
import ru.itis.models.Course;

import java.util.List;

/**
 * 18.12.2021
 * 17. Spring Data JPA
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CoursesRepository {
    Course save(Course course);
    List<Course> findAllByLesson_name(String name);
}
