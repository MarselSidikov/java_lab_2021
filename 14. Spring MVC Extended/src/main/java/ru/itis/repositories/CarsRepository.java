package ru.itis.repositories;

import ru.itis.models.Car;

/**
 * 27.11.2021
 * 14. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CarsRepository extends CrudRepository<Car> {
}
