package ru.itis.shop.services;

import ru.itis.shop.dto.StudentDto;

import java.util.List;

/**
 * 17.02.2022
 * 18. Spring Boot Demo
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface StudentsService {
    List<StudentDto> getAll();
}
