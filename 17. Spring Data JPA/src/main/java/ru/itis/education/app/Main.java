package ru.itis.education.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itis.education.config.ApplicationConfig;
import ru.itis.education.models.Course;
import ru.itis.education.repositories.CoursesRepository;
import ru.itis.education.repositories.CoursesRepositoryJpaImpl;

/**
 * 18.12.2021
 * 17. Spring Data JPA
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        CoursesRepository coursesRepository = context.getBean(CoursesRepository.class);
        Course course = Course.builder()
                .name(".NET")
                .description("Изучаем основы разработки систем на .NET")
                .build();
        coursesRepository.save(course);
    }
}
