package ru.itis.services;

import ru.itis.dto.CourseDto;

import java.util.List;

/**
 * 18.12.2021
 * 14. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CoursesService {
    List<CourseDto> getCoursesByLessonName(String lessonName);

    CourseDto addCourse(CourseDto course);
}
