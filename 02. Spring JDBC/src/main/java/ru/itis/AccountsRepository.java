package ru.itis;

import ru.itis.models.Account;

import java.util.List;
import java.util.Optional;

/**
 * 03.07.2021
 * 01. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface AccountsRepository {
//    List<Account> findAll();

    List<Account> findAllByFirstName(String name);
    Optional<Account> findById(Integer id);
    Optional<Account> findByIdWithCars(Integer id);
    void save(Account account);

//    List<Account> findAllByFirstNameOrLastNameLike(String name);
//    void update(Account account);
}
