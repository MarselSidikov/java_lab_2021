package ru.itis;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.itis.models.Account;

import javax.sql.DataSource;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.Optional;
import java.util.Properties;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)  {

        Properties properties = new Properties();

        try {
            properties.load(new FileReader("resources\\application.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

//        DataSource dataSource = new SimpleDataSource(properties);

        HikariConfig config = new HikariConfig();
        config.setDriverClassName(properties.getProperty("db.driver"));
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setUsername(properties.getProperty("db.user"));
        config.setPassword(properties.getProperty("db.password"));
        config.setMaximumPoolSize(Integer.parseInt(properties.getProperty("db.hikari.pool-size")));

        DataSource dataSource = new HikariDataSource(config);

        AccountsRepository accountsRepository = new AccountsRepositoryJdbcImpl(dataSource);
        Optional<Account> optionalAccount = accountsRepository.findById(1);

        for (Account account : accountsRepository.findAll()) {
            System.out.println(account);
        }

//        Account marsel = optionalAccount.orElseThrow(IllegalArgumentException::new);
//
//        marsel.setFirstName("Марс");
//        marsel.setLastName("Ситдиков");
//        marsel.setAge(29);
//        accountsRepository.update(marsel);
//        Scanner scanner = new Scanner(System.in);
//        scanner.nextLine();
//        for (int i = 0; i < 1000; i++) {
//            Thread thread = new Thread(() -> System.out.println(accountsRepository.findAll()));
//            thread.start();
//        }
    }
}
