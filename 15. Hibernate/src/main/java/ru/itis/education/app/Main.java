package ru.itis.education.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import ru.itis.education.models.Course;
import ru.itis.education.models.Student;

import java.util.List;

/**
 * 05.12.2021
 * 15. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        Course course = session.find(Course.class, 3L);

        Query<Student> studentQuery = session.createQuery(
                "select student from Student student left join student.courses as course where course.lessons.size > 2",
                Student.class);

        List<Student> students = studentQuery.getResultList();

//        System.out.println(course);
        System.out.println(students);

        sessionFactory.close();

    }
}
