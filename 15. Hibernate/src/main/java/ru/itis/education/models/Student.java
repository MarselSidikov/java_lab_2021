package ru.itis.education.models;

import lombok.*;

import java.util.List;

/**
 * 05.12.2021
 * 15. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
//@ToString(exclude = "courses")
@EqualsAndHashCode(exclude = "courses")
public class Student {
    private Long id;

    private String firstName;
    private String lastName;

    private List<Course> courses;
}
