package ru.itis.services;

import ru.itis.dto.AuthorDto;
import ru.itis.dto.PostDto;

import java.util.List;

/**
 * 25.12.2021
 * 14. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface BlogService {
    PostDto addPost(Long authorId, PostDto post);

    AuthorDto addAuthor(AuthorDto authorDto);

    List<PostDto> searchPostsByTitle(String title);
}
