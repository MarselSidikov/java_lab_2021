# Maven

* `build lifecycle`
  * `clean`
    * `clean` - `phase`
  * `default`
    * `validate` - `phase`
    * `compile` - `phase`
    * `test` - `phase`
    * `package` - `phase`
    * `verify` - `phase`
    * `install` - `phase`
  * `site` 
    * `site` - `phase`
* `phase`
* `goal of plugin`