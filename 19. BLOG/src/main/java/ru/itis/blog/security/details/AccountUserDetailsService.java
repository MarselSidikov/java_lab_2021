package ru.itis.blog.security.details;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.itis.blog.models.Account;

/**
 * 10.11.2021
 * 42. Spring Boot Security - MVC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RequiredArgsConstructor
@Service
public class AccountUserDetailsService implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        if (email.equals("sidikov.marsel@gmail.com")) {

            Account account = Account.builder()
                    .email("sidikov.marsel@gmail")
                    .id(10L)
                    .firstName("Marsel")
                    .lastName("Sidikov")
                    .role(Account.Role.ADMIN)
                    .state(Account.State.CONFIRMED)
                    .password("$2a$10$sEu.aBYqS/VmnyXpx6IRCO6vu5HdcbgwfKYNqgjduwcACPUpVRcXu")
                    .build();

            return new AccountUserDetails(account);
        } else throw new UsernameNotFoundException("User not found");
    }

//    private final AccountsRepository accountsRepository;
//
//    @Override
//    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
//        return new AccountUserDetails(
//                accountsRepository.findByEmail(email)
//                .orElseThrow(
//                        () -> new UsernameNotFoundException("User not found")));
//    }

}
