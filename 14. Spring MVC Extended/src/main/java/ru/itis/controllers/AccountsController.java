package ru.itis.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.itis.dto.AccountDto;
import ru.itis.services.AccountsService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 06.11.2021
 * 13. Spring MVC
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RequiredArgsConstructor
@Controller
@RequestMapping("/accounts")
public class AccountsController {

    private final AccountsService accountsService;

    @RequestMapping(method = RequestMethod.GET)
    public String getAccountsPage(Model model) {
        model.addAttribute("accounts", accountsService.getAccounts());
        return "accounts";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String addAccount(AccountDto account) {
        accountsService.addAccount(account);
        return "redirect:/accounts";
    }

    @RequestMapping(value = "/{account-id}", method = RequestMethod.GET)
    public String getAccountPage(@PathVariable("account-id") Long accountId, Model model) {
        model.addAttribute("account", accountsService.getAccount(accountId));
        return "account";
    }
}
