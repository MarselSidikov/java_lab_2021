package ru.itis.shop.services;

import ru.itis.shop.dto.SignUpDto;

/**
 * 24.02.2022
 * 18. Spring Boot Demo
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface SignUpService {
    void signUp(SignUpDto accountForm);
}
