package ru.itis;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.itis.models.Account;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 03.07.2021
 * 01. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class AccountsRepositoryJdbcTemplateImpl implements AccountsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select c.id as car_id, * from account a left join car c on c.owner_id = a.id order by a.id";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_FIRST_NAME = "select * from account where first_name = ?";

    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update account set first_name = ?, last_name = ?, age = ? where id = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_ID_WITHOUT_CARS = "select * from account where account.id = ?";

    //language=SQL
    private static final String SQL_INSERT = "insert into account(first_name, last_name, age) values (?, ?, ?)";

    private JdbcTemplate jdbcTemplate;

    public AccountsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private final RowMapper<Account> accountRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
            String firstName = row.getString("first_name");
            String lastName = row.getString("last_name");
            int age = row.getInt("age");

            Account account = new Account(id, firstName, lastName, age);
            account.setCars(new ArrayList<>());
            return account;
    };

    private final ResultSetExtractor<Account> accountResultSetExtractor = resultSet -> null;

    @Override
    public List<Account> findAllByFirstName(String searchFirstName) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_FIRST_NAME, accountRowMapper, searchFirstName);
    }

    @Override
    public Optional<Account> findById(Integer id) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_SELECT_BY_ID_WITHOUT_CARS, accountRowMapper, id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }


    @Override
    public Optional<Account> findByIdWithCars(Integer id) {
        return null;
    }

    @Override
    public void save(Account account) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT, new String[] {"id"});

            statement.setString(1, account.getFirstName());
            statement.setString(2, account.getLastName());
            statement.setInt(3, account.getAge());

            return statement;
        }, keyHolder);

        account.setId(keyHolder.getKey().intValue());
    }


}
