package ru.itis.shop.services;

import org.springframework.web.multipart.MultipartFile;
import ru.itis.shop.dto.FileLinkDto;

import javax.servlet.http.HttpServletResponse;

/**
 * 17.02.2022
 * 18. Spring Boot Demo
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface FilesService {
    FileLinkDto upload(MultipartFile multipart, String description);

    void addFileToResponse(String fileName, HttpServletResponse response);
}
