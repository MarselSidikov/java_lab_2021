package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.models.Post;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 25.12.2021
 * 14. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface PostsRepository extends JpaRepository<Post, Long> {
    List<Post> findAllByTitleLike(String title);
    List<Post> findAllByAuthor_FirstNameLike(String firstName);
    List<Post> findAllByPublishedTimeBefore(LocalDateTime dateTime);
}
