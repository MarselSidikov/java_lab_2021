import java.time.LocalDateTime;

/**
 * 04.09.2021
 * 03. Reflection
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ObjectsGenerator generator = new ObjectsGenerator();
        User user = generator.generate(User.class, "Марсель", "Сидиков", 27, 1.85);
        Certificate certificate = generator.generate(Certificate.class, LocalDateTime.now(),
                "COVID-19", 100);
        System.out.println(user);
        System.out.println(certificate);
    }
}
