package ru.itis.education.models;

import lombok.*;

import java.util.List;

/**
 * 05.12.2021
 * 15. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = "students")
public class Course {
    private Long id;

    private String name;

    private String description;

    private List<Lesson> lessons;

    private List<Student> students;
}
