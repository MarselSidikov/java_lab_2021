package ru.itis.blog.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.blog.dto.AddPostDto;
import ru.itis.blog.dto.PostDto;
import ru.itis.blog.services.PostsService;

/**
 * 31.03.2022
 * 19. BLOG
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/posts")
public class PostsController {

    private final PostsService postsService;

    @GetMapping("/{post-id}")
    public ResponseEntity<PostDto> getPost(@PathVariable("post-id") Long postId) {
        return ResponseEntity.ok(postsService.getPost(postId));
    }

    @PostMapping
    public ResponseEntity<PostDto> addPost(@RequestBody AddPostDto post) {
        return ResponseEntity.ok(postsService.addPost(post));
    }
}
