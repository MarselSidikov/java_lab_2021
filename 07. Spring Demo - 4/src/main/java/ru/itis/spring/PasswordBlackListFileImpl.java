package ru.itis.spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * 04.09.2021
 * 04. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Component("passwordBlackListFile")
public class PasswordBlackListFileImpl implements PasswordBlackList {

    private final String fileName;

    public PasswordBlackListFileImpl(@Value("${passwordBlackList.file}")String fileName) {
        this.fileName = fileName;
    }

    @Override
    public boolean contains(String password) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().anyMatch(line -> line.equals(password));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
