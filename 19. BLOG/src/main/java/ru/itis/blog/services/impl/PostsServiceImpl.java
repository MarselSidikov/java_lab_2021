package ru.itis.blog.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.blog.dto.AddPostDto;
import ru.itis.blog.dto.PostDto;
import ru.itis.blog.exceptions.AuthorNotExistsException;
import ru.itis.blog.exceptions.PostNotFoundException;
import ru.itis.blog.models.Author;
import ru.itis.blog.models.Post;
import ru.itis.blog.repositories.AuthorsRepository;
import ru.itis.blog.repositories.PostsRepository;
import ru.itis.blog.services.PostsService;

import java.time.LocalDateTime;
import java.util.function.Supplier;

import static ru.itis.blog.dto.PostDto.from;

/**
 * 31.03.2022
 * 19. BLOG
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@RequiredArgsConstructor
@Service
public class PostsServiceImpl implements PostsService {

    private final PostsRepository postsRepository;

    private final AuthorsRepository authorsRepository;

    @Override
    public PostDto getPost(Long postId) {
        return from(postsRepository.findById(postId).orElseThrow(PostNotFoundException::new));
    }

    @Override
    public PostDto addPost(AddPostDto post) {
        Author author = authorsRepository
                .findById(post.getAuthorId())
                .orElseThrow((Supplier<RuntimeException>) ()
                        -> new AuthorNotExistsException(post.getAuthorId()));

        Post newPost = Post.builder()
                .createdAt(LocalDateTime.now())
                .state(Post.State.PUBLISHED)
                .text(post.getText())
                .title(post.getTitle())
                .author(author)
                .build();

        return from(postsRepository.save(newPost));
    }
}
